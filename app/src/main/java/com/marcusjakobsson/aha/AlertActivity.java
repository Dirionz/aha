package com.marcusjakobsson.aha;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.oralb.sdk.OBTBrush;
import com.oralb.sdk.OBTSDK;

import java.util.Timer;


public class AlertActivity extends MyOBTBrushListener{

    private static CountDownTimer timer;
    private static CountDownTimer timerBrush;
    private final long halfHour = 30*60*1000;
    private final long hour = 60*60*1000;
    private final long fiftyfiveMinutes=115*60*1000;
    private final long fiveMinues=5*60*1000;

    private boolean doneBrushing = false;
    private long brushTime = 0;
    private final long validBrushingSession = 30;

    Ringtone ringtone;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_wait);
        setContentView(R.layout.activity_alert);

        //Samtliga flaggor tillåter denna aktivitet att visas när skärmen är släckt i locked mode
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        timerBrush = new CountDownTimer(fiftyfiveMinutes, fiveMinues) {
            private int count = 0;
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("timerBrush", "onTick playing ringtone.");
                count++;
                if (fiftyfiveMinutes/fiveMinues*count != 1) {
                    return;
                }
                //Constants.getRingtone(AlertActivity.this).play();
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    ringtone.play();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                Log.d("timerBrush", "onFinish stopping alarm.");
                //stopAlarm();
                //Constants.getRingtone(AlertActivity.this).stop();
                if (ringtone != null) {
                    ringtone.stop();
                }
            }
        }.start();

        timer = new CountDownTimer(hour, halfHour)
        {

            @Override
            public void onTick(long millisUntilFinished)
            {
                Log.i("TICK", "Tick "+millisUntilFinished);
                //timerBrush.start();
            } //Kan användas för att skriva ut varje tick

            @Override
            public void onFinish() //När timern är färdig kommer följande exekveras
            {
                Log.i("TICK", "Finished!");
                if (!OBTSDK.isConnected()) {
                    stopAlarm();
                    EventLog.addEvent(AlertActivity.this, "Alarm missat.");
                }
            }

        }.start();
    }//End onCreate



    private void stopAlarm()
    {
        if (ringtone != null) {
            ringtone.stop();
        }
        if (dialog != null) {
            dialog.dismiss();
        }
        timer.cancel();
        timerBrush.cancel();

        Intent intent = new Intent(getApplicationContext(), FinalActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBrushStateChanged(int i) {
        super.onBrushStateChanged(i);
        Log.i("onBrushStateChanged", Integer.toString(i));
        switch (i) {
            case OBTBrush.State.BRUSHING:
                break;
            case OBTBrush.State.CHARGER:
            case OBTBrush.State.IDLE:
                if (doneBrushing) {
                    OBTSDK.disconnectToothbrush();
                } else {
                    brushTimeNotReached();
                }
                break;
        }
    }

    private void brushTimeNotReached() {
        if (dialog != null) {
            //dialog.dismiss();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Du uppnådde inte den rekommenderade tandborstningstiden på minst 30 sekunder. Försök igen.");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBrushDisconnected() {
        Log.i("BRUSH", "Brush disconnected");
        stopAlarm();
        if(doneBrushing) {
            EventLog.addEvent(this, "Tandbostsession slutförd", brushTime);
        } else {
            EventLog.addEvent(this, "Tandbostsessionens minsta tid ej uppnådd", brushTime);
        }
    }


    @Override
    public void onBrushingTimeChanged(long l) { // TODO: Stop the alarming here??
        Log.i("TIME", String.valueOf(l/1000));
        brushTime = l/1000;
        //TODO ladda upp användarens ej slutgiltiga eller slutgiltiga tb-tider.
        if (l/1000 > validBrushingSession)
        {
            doneBrushing = true; //En godkänd tb-session
        }

    }




    @Override
    public void onBackPressed() {
    }
}//End AlertActivity
