package com.marcusjakobsson.aha;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by christofferassgard on 2/4/18.
 */

public class EventLog {
    private static String SP_EVENTS = "sp_events";
    private static List<Event> events = null;

    public static class Event implements Serializable {
        String text;
        long timeInMillis;
        int brushTime;

        Event(String text, int brushTime) {
            this.text = text;
            this.timeInMillis = System.currentTimeMillis();
            this.brushTime = brushTime;
        }

    }

    public static void addEvent(Context context, String text) {
        addEvent(context, text, 0);
    }

    public static void addEvent(Context context, String text, long brushTime) {
        SharedPreferences pref = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        if (events == null) {
            String json = pref.getString(SP_EVENTS, "");
            if (json.length() > 0) {
                Gson gson = new Gson();
                events = gson.fromJson(json, new TypeToken<List<Event>>() {}.getType());
            } else {
                events = new ArrayList<>();
            }
        }

        events.add(new Event(text, (int)brushTime));

        Gson gson = new Gson();
        String json = gson.toJson(events);
        pref.edit().putString(SP_EVENTS, json).apply();
    }

    public static List<Event> getEvents(Context context) {
        SharedPreferences pref = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        if (events == null) {
            String json = pref.getString(SP_EVENTS, "");
            if (json.length() > 0) {
                Gson gson = new Gson();
                events = gson.fromJson(json, new TypeToken<List<Event>>() {}.getType());
            } else {
                events = new ArrayList<>();
            }
        }

        return events;
    }

}
