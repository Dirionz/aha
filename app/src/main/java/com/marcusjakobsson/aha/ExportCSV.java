package com.marcusjakobsson.aha;

import android.util.Log;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by christofferassgard on 2/9/18.
 */

public class ExportCSV {

    public static void exportEvents(List<EventLog.Event> events) throws IOException {

        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileName = "Events.csv";


        String filePath = baseDir + File.separator + fileName;
        File f = new File(filePath);

        FileOutputStream os = new FileOutputStream(f);
        os.write(0xef);
        os.write(0xbb);
        os.write(0xbf);
        CSVWriter writer = new CSVWriter(new OutputStreamWriter(os));

        {
            String[] data = {"Datum", "Typ", "Antal sekunder"};
            writer.writeNext(data);
        }

        for (EventLog.Event event : events) {
            String dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.GERMANY).format(new Date(event.timeInMillis));
            Log.i("dateFormat", dateFormat);
            String sec = (event.brushTime > 0) ? Integer.toString(event.brushTime) : "";
            String[] data = {dateFormat, event.text, sec};
            writer.writeNext(data);
        }

        writer.close();
    }
}
