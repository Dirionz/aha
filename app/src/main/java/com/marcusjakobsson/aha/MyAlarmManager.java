package com.marcusjakobsson.aha;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import java.util.Calendar;
import java.util.TimeZone;

import android.util.Log;


class MyAlarmManager{
    //private final PendingIntent pendingIntent;
    private final AlarmManager alarm;
    private int hours;
    private int minutes;
    private int hoursw;
    private int minutesw;

    private final Context myContext;
    private final int myId;




    MyAlarmManager(Context context, String time, String wtime, int id){
        alarm = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        //pendingIntent = PendingIntent.getBroadcast(context, id, intentRecv, 0);
        myContext = context;
        myId = id;
        toIntTime(time, wtime);

        //setUpAlarm();
        setUpAlarms();
    }//End of onCreate




//    /**
//     * Ställer in alarmet utifrån de privata medlemmarnas data
//     * och skapar ett Calendar-objekt i tidszonen GMT (Europa/Stockholm) för att inte få tidsförskjutning
//     */
//    private void setUpAlarm(){
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//        cal.setTimeZone(TimeZone.getTimeZone("Europe/Stockholm"));
//
//        //Kontrollerar att tiden inte har passerat den angivna tiden.
//        Log.i("HOUR", String.valueOf(hours));
//        if( hours == 24 || cal.get(Calendar.HOUR_OF_DAY) > hours)
//            cal.add(Calendar.DAY_OF_YEAR, 1);
//        else if(cal.get(Calendar.HOUR_OF_DAY) == hours)
//        {
//            if(cal.get(Calendar.MINUTE) > minutes)
//                cal.add(Calendar.DAY_OF_YEAR, 1);
//        }
//
//        cal.set(Calendar.HOUR_OF_DAY, hours);
//        cal.set(Calendar.MINUTE, minutes);
//        Log.i("Alarm set to"+myId, cal.getTime().toString());
//        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
//        Intent intentFinal = new Intent(myContext,FinalActivity.class);
//        myContext.startActivity(intentFinal);
//    }//End of setUpAlarm

    private void scheduleAlarm(int dayOfWeek, int h, int m) {

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.setTimeZone(TimeZone.getTimeZone("Europe/Stockholm"));

        Calendar systemCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        systemCal.setTimeZone(TimeZone.getTimeZone("Europe/Stockholm"));

        Log.i("HOUR", String.valueOf(hours));
        if( hours == 24 || cal.get(Calendar.HOUR_OF_DAY) > hours)
            cal.add(Calendar.DAY_OF_YEAR, 1);
        else if(cal.get(Calendar.HOUR_OF_DAY) == hours)
        {
            if(cal.get(Calendar.MINUTE) > minutes)
                cal.add(Calendar.DAY_OF_YEAR, 1);
        }

        cal.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        cal.set(Calendar.HOUR_OF_DAY, h);
        cal.set(Calendar.MINUTE, m);

        if (cal.getTimeInMillis() < systemCal.getTimeInMillis()) {
            long newMillis = cal.getTimeInMillis() + AlarmManager.INTERVAL_DAY * 7;
            cal.setTimeInMillis(newMillis);
        }

        int id_ = (int) myId+dayOfWeek;
        Log.i("Alarm set to "+id_, cal.getTime().toString());

        Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, id_, intentRecv, 0);

        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);
    }

    private void setUpAlarms() {

        scheduleAlarm(Calendar.MONDAY, hours, minutes);
        scheduleAlarm(Calendar.TUESDAY, hours, minutes);
        scheduleAlarm(Calendar.WEDNESDAY, hours, minutes);
        scheduleAlarm(Calendar.THURSDAY, hours, minutes);
        scheduleAlarm(Calendar.FRIDAY, hours, minutes);

        scheduleAlarm(Calendar.SATURDAY, hoursw, minutesw);
        scheduleAlarm(Calendar.SUNDAY, hoursw, minutesw);

        Intent intentFinal = new Intent(myContext,FinalActivity.class);
        myContext.startActivity(intentFinal);
    }




    /**
     * Tar in en sträng i formatet "HH:MM" och bryter ut timmar samt minuter
     * och placerar dessa i de privata variablerna hours och minutes
     */
    private void toIntTime(String time, String wtime) {
        String [] timeArr = time.split(":");
        hours = Integer.parseInt(timeArr[0]);
        minutes = Integer.parseInt(timeArr[1]);

        String [] timeArrw = wtime.split(":");
        hoursw = Integer.parseInt(timeArrw[0]);
        minutesw = Integer.parseInt(timeArrw[1]);
    }//End of toIntTime




    void stopAlarm(){
        //alarm.cancel(pendingIntent);
        cancelAlarms();
    }//End of stopAlarm

    private void cancelAlarms() {

        {
            Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, myId+Calendar.MONDAY, intentRecv, 0);
            alarm.cancel(pendingIntent);
        }
        {
            Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, myId+Calendar.TUESDAY, intentRecv, 0);
            alarm.cancel(pendingIntent);
        }
        {
            Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, myId+Calendar.WEDNESDAY, intentRecv, 0);
            alarm.cancel(pendingIntent);
        }
        {
            Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, myId+Calendar.THURSDAY, intentRecv, 0);
            alarm.cancel(pendingIntent);
        }
        {
            Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, myId+Calendar.FRIDAY, intentRecv, 0);
            alarm.cancel(pendingIntent);
        }

        {
            Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, myId+Calendar.SATURDAY, intentRecv, 0);
            alarm.cancel(pendingIntent);
        }
        {
            Intent intentRecv = new Intent(myContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(myContext, myId+Calendar.SUNDAY, intentRecv, 0);
            alarm.cancel(pendingIntent);
        }

    }

}//End MyAlarmManager
